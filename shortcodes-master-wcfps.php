<?php
/*
  Plugin Name: Shortcodes Master - Woocommerce featured products slider
  Plugin URI: https://lizatom.com/wordpress-woocommerce-slider/
  Version: 1.0.0
  Author: Lizatom.com
  Author URI: https://lizatom.com
  Description: Woocommerce featured products slider with skins.
  Text Domain: smwcfps
  Domain Path: /lang
 */

define( 'SMWCFPS_PLUGIN_FILE', __FILE__ );
define( 'SMWCFPS_PLUGIN_VERSION', '1.0.0' );

require_once 'inc/wcfps.php';
require_once 'inc/shortcodes.php';
