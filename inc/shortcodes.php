<?php
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

class Shortcodes_Master_Wcfps_Shortcodes
    {
        
    function __construct() { }

/**
 * wcfps
 */
    public static function wcfps($atts = null, $content = null)
        {        	
        	if(!is_plugin_active('woocommerce/woocommerce.php')) return;
        $atts=shortcode_atts(array
            (
            'id' => 'wcfps-unique-id',
            'autoplay' => 'yes',
            'slides' => '3',
            'excerpt_lenght' => '30',
            'gradient_color1' => '#ffffff',
            'gradient_color2' => '#ECF0F1',
            'title_color' => '#777777',
            'excerpt_color' => '#95A5A6',
            'regularprice_color' => '#27AE60',
            'saleprice_color' => '#C0392B',            
            'button_color' => '#ad74a2',
            'buttontext_color' => '#ECF0F1',
            'navigation' => 'yes',
            'pagination' => 'yes',
            'transition' => 'fade'
            ), $atts, 'wcfps');

        sm_query_asset( 'css', 'font-awesome' );
        sm_query_asset( 'css', 'owl-carousel' );
        sm_query_asset( 'css', 'wcfps-theme-default' );
        sm_query_asset( 'css', 'owl-transitions' );
        sm_query_asset( 'css', 'wcfps' );
        sm_query_asset( 'js','jquery' );
        sm_query_asset( 'js','owl-carousel' );
        sm_query_asset( 'js','wcfps' );

        $autoplay = ($atts['autoplay'] === "yes" ? "true" : "false");
        $navigation = ($atts['navigation'] === "yes" ? "true" : "false");
        $pagination = ($atts['pagination'] === "yes" ? "true" : "false");       
    
global $wpdb, $product;
$my_attachment_objects = array();

$args = array(
    'post_type' => 'product',
    'meta_key' => '_featured',
    'meta_value' => 'yes',
    'posts_per_page' => $atts['slides']
);

$featured_query = new WP_Query( $args );
    
    $return = '<div id="'.$atts['id'].'" class="owl-carousel wcfps-theme-default" style="background-image: radial-gradient(circle at top right, '.$atts['gradient_color1'].', '.$atts['gradient_color2'].' 50% );">';
    if ($featured_query->have_posts()) : 

    while ($featured_query->have_posts()) : 
    
        $featured_query->the_post();
        
        $product = get_product( $featured_query->post->ID );
        
        // Output product information here
        /*echo "<pre>";
        print_r($product);
        echo "</pre>";*/        

        // arguments for get_posts
        global $post;
    $attachment_args = array(
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'post_status' => null, // attachments don't have statuses
        'post_parent' => $post->ID
    );
    // get the posts
    $this_posts_attachments = get_posts( $attachment_args );
    // append those posts onto the array
    $my_attachment_objects[$post->ID] = $this_posts_attachments;

    $wcfps_excerpt = $atts['excerpt_lenght']>0 ? '<div class="wcfps-excerpt" style="color: '.$atts['excerpt_color'].';">'.substr(get_the_excerpt(),0,$atts['excerpt_lenght']).'...</div>' : '';

    if(get_post_meta( get_the_ID(), '_sale_price')) { $sale_price = '<span class="wcfps-price-sale" style="color: '.$atts['saleprice_color'].';">'.get_woocommerce_currency_symbol().''.get_post_meta( get_the_ID(), '_sale_price').'</span>'; } else { $sale_price = ''; }
        $return .= '<div class="item">
            <div class="wcfps-image">

            <div class="wcfps-wrap-image">
            
            <img src="'.wp_get_attachment_url( get_post_thumbnail_id($post->ID)).'">
            <div class="clear"></div>
            </div><div class="clear"></div>
            </div>

            <div class="wcfps-details">
            <h2 class="wcfps-title" style="color: '.$atts['title_color'].';">'.get_the_title().'</h2>
            '.$wcfps_excerpt.'
            <div class="wcfps-price">
            '.$sale_price.'
            <span class="wcfps-price-regular" style="color: '.$atts['regularprice_color'].';">'.get_woocommerce_currency_symbol().''.get_post_meta( get_the_ID(), '_regular_price').'</span>                
            </div>
            <div class="wcfps-permalink"><a href="'.get_permalink().'" style="color: '.$atts['buttontext_color'].'; background-color: '.$atts['button_color'].';">More info</a></div>
            </div>
            <div class="clear"></div>
        </div>';

        endwhile;
    
endif;

wp_reset_query(); // Remember to reset

        $return .= '</div>';
        $return .= '<script  type="text/javascript">jQuery(document).ready(function() {
            var owl = jQuery("#'.$atts['id'].'"); owl.owlCarousel({
                baseClass : "owl-carousel",
                theme : "wcfps-theme-default",
                navigation : '.$navigation.', 
                navigationText: true,
                navigationText : ["<i class=\'fa fa-chevron-left\'><i>","<i class=\'fa fa-chevron-right\'><i>"], 
                pagination: '.$pagination.',                 
                singleItem : true, 
                transitionStyle : "'.$atts['transition'].'",
                autoHeight: true,
                responsive: true,
                responsiveRefreshRate : 200,
                responsiveBaseWidth: window,
                autoPlay : '.$autoplay.'
        }); });</script>';
        return $return;
        }
        
    }