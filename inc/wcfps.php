<?php
class Shortcodes_Master_Wcfps {

	/**
	 * Constructor
	 */
	function __construct() {
		// Load textdomain
		load_plugin_textdomain( 'smwcfps', false, dirname( plugin_basename( SMWCFPS_PLUGIN_FILE ) ) . '/lang/' );
		// Reset cache on activation
		if ( class_exists( 'Sm_Generator' ) ) register_activation_hook( SMWCFPS_PLUGIN_FILE, array( 'Sm_Generator', 'reset' ) );
		// Init plugin
		add_action( 'plugins_loaded', array( __CLASS__, 'init' ), 20 );
		// Make pluin meta translation-ready
		__( 'ShortcodesMaster', 'smwcfps' );
		__( 'Shortcodes Master - Woocommerce featured products slider', 'smwcfps' );
		__( 'Woocommerce featured products slider addon for Shortcodes Master.', 'smwcfps' );
	}

	/**
	 * Plugin init
	 */
	public static function init() {
		// Check for SM
		if ( !function_exists( 'shortcodes_master' ) ) {
			// Show notice
			add_action( 'admin_notices', array( __CLASS__, 'sm_notice' ) );
			// Break init
			return;
		}
		// Register assets
		add_action( 'init', array( __CLASS__, 'assets' ) );
		// Add new group to Generator
		add_filter( 'sm/data/groups', array( __CLASS__, 'group' ) );
		// Register new shortcodes
		add_filter( 'sm/data/shortcodes', array( __CLASS__, 'data' ) );
		// Add plugin meta links
		add_filter( 'plugin_row_meta', array( __CLASS__, 'meta_links' ), 10, 2 );
	}

	/**
	 * Install SM notice
	 */
public static function sm_notice() {
?><div class="updated">
			<p><?php _e( 'Please install and activate latest version of <b>Shortcodes Master</b> to use it\'s addon <b>Shortcodes Master - Woocommerce featured products
slider</b>.<br />Deactivate this addon to hide this message.', 'smwcfpc' ); ?></p>
			<p><a href="https://lizatom.com/wordpress-shortcodes-plugin/" target="_blank" class="button button-primary"><?php _e( 'Install Shortcodes Master', 'smwcfpc' ); ?> &rarr;</a></p>	
			</div><?php
	}

	public static function assets() { 
        $plugins_url = plugins_url();

		wp_register_style( 'owl-carousel', plugins_url( 'assets/owl-carousel/owl.carousel.css', SMWCFPS_PLUGIN_FILE ), false, SMWCFPS_PLUGIN_VERSION, 'all' );
        wp_register_style( 'wcfps-theme-default', plugins_url( 'assets/owl-carousel/wcfps.theme.default.css', SMWCFPS_PLUGIN_FILE ), false, SMWCFPS_PLUGIN_VERSION, 'all' );
        wp_register_style( 'owl-transitions', plugins_url( 'assets/owl-carousel/owl.transitions.css', SMWCFPS_PLUGIN_FILE ), false, SMWCFPS_PLUGIN_VERSION, 'all' );
        wp_register_style( 'wcfps', plugins_url( 'assets/css/wcfps.css', SMWCFPS_PLUGIN_FILE ), false, SMWCFPS_PLUGIN_VERSION, 'all' );
        
        wp_register_script('jquery', includes_url('js/jquery/jquery.js'));       
        wp_register_script( 'owl-carousel', plugins_url( 'assets/owl-carousel/owl.carousel.min.js', SMWCFPS_PLUGIN_FILE ), array('jquery'), SMWCFPS_PLUGIN_VERSION, true );
        wp_register_script( 'wcfps', plugins_url( 'assets/js/wcfps.js', SMWCFPS_PLUGIN_FILE ), array('jquery'), SMWCFPS_PLUGIN_VERSION, true );
        
	}

	public static function meta_links( $links, $file ) {
		if ( $file === plugin_basename( SMWCFPS_PLUGIN_FILE ) ) $links[] = '<a href="https://lizatom.com/wiki/shortcodes-master-woocommerce-slider/" target="_blank">' . __( 'Help', 'smwcfps' ) . '</a>';
		return $links;
	}

	/**
	 * Add new group to the Generator
	 */
	public static function group( $groups ) {
		$groups['woocommerce'] = __( 'Woocommerce', 'smwcfps' );
		return $groups;
	}

	/**
	 * New shortcodes data
	 */
	public static function data( $shortcodes ) {
        
        /*wcfpss*/
        
		$shortcodes['wcfps'] = array(
			'name'     => __( 'Woocommerce FP slider', 'smwcfps' ),
			'type'     => 'single',
			'group'    => 'gallery',
            'content' =>  __( "[%prefix_wcfps id=\"unique-wcfps-id\"]", 'smwcfps' ),             
			'desc'     => __( 'Woocommerce featured products slider.', 'smwcfps' ),
			'note'     => sprintf( __( 'Read more on this shortcode %s here %s', 'sm' ), '<a href="https://lizatom.com/wiki/shortcodes-master-woocommerce-slider/" target="_blank">', '&rarr;</a>' ),
			'icon'     => 'picture-o',
			'function' => array( 'Shortcodes_Master_Wcfps_Shortcodes', 'wcfps' ),
	            'atts' => array(
                        'id' => array(
                            'default' => 'unique-wcfps-id',
                            'name' => __( 'ID', 'smwcfps' ),
                            'desc' => __( 'Required! Insert unique id of the slider. Example: unique-wcfps-id', 'smwcfps')),
                        'autoplay' => array(
                                        'type' => 'bool',
                                        'default' => 'yes',
                                        'name' => __( 'Autoplay', 'smm' ),
                                        'desc' => __( 'Run the slider automatically.', 'smwcfps' )
                        ),
                        'slides' => array(
                            'type' => 'slider',
                            'min' => 1,
                            'max' => 20,
                            'step' => 1,
                            'default' => 3,
                            'name' => __( 'Slides', 'smwcfps' ),
                            'desc' => __( 'Number of slides.', 'smwcfps' )
                        ),
                        'excerpt_lenght' => array(
                            'type' => 'slider',
                            'min' => 0,
                            'max' => 1000,
                            'step' => 1,
                            'default' => 30,
                            'name' => __( 'Excerpt length', 'smwcfps' ),
                            'desc' => __( 'Excerpt lenght (characters). Set to 0 to disable excerpt.', 'smwcfps' )
                        ),          
                        'gradient_color1' => array(
                                        'type'    => 'color',
                                        'default' => '#ffffff',
                                        'name'    => __( 'Gradient color #1', 'smwcfps' ),
                                        'desc'    => __( 'Gradient color #1.', 'smwcfps' )
                        ),            
                        'gradient_color2' => array(
                                        'type'    => 'color',
                                        'default' => '#ECF0F1',
                                        'name'    => __( 'Gradient color #2', 'smwcfps' ),
                                        'desc'    => __( 'Gradient color #2', 'smwcfps' )
                        ),        
                        'title_color' => array(
                                        'type'    => 'color',
                                        'default' => '#777777',
                                        'name'    => __( 'Title color', 'smwcfps' ),
                                        'desc'    => __( 'Color of the title.', 'smwcfps' )
                        ),
                        'excerpt_color' => array(
                                        'type'    => 'color',
                                        'default' => '#95A5A6',
                                        'name'    => __( 'Excerpt color', 'smwcfps' ),
                                        'desc'    => __( 'Color of the excerpt.', 'smwcfps' )
                        ),
                        'regularprice_color' => array(
                                        'type'    => 'color',
                                        'default' => '#27AE60',
                                        'name'    => __( 'Regular price color', 'smwcfps' ),
                                        'desc'    => __( 'Color of the regular price.', 'smwcfps' )
                        ),
                        'saleprice_color' => array(
                                        'type'    => 'color',
                                        'default' => '#C0392B',
                                        'name'    => __( 'Sale price color', 'smwcfps' ),
                                        'desc'    => __( 'Color of the sale price.', 'smwcfps' )
                        ),
                        'button_color' => array(
                                        'type'    => 'color',
                                        'default' => '#ad74a2',
                                        'name'    => __( 'Button color', 'smwcfps' ),
                                        'desc'    => __( 'Color of the button.', 'smwcfps' )
                        ),
                        'buttontext_color' => array(
                                        'type'    => 'color',
                                        'default' => '#ECF0F1',
                                        'name'    => __( 'Button text color', 'smwcfps' ),
                                        'desc'    => __( 'Color of the button text.', 'smwcfps' )
                        ),
                        'navigation' => array(
                                        'type' => 'bool',
                                        'default' => 'yes',
                                        'name' => __( 'Navigation', 'smwcfps' ),
                                        'desc' => __( 'Display arrow navigation?', 'smwcfps' )
                        ),
                        'pagination' => array(
                                        'type' => 'bool',
                                        'default' => 'yes',
                                        'name' => __( 'Pagination', 'smwcfps' ),
                                        'desc' => __( 'Display pagination buttons?', 'smwcfps' )
                        ),
                        'transition' => array(
                                        'type' => 'select',
                                        'values' => array(
                                                'fade'   => __( 'fade', 'smwcfps' ),
                                                'backSlide'   => __( 'backSlide', 'smwcfps' ),
                                                'goDown'   => __( 'goDown', 'smwcfps' ),
                                                'fadeUp'   => __( 'fadeUp', 'smwcfps' )
                                            ),
                                        'default' => 'fade',
                                        'name' => __( 'Transition', 'smwcfps' ),
                                        'desc' => __( 'Transition effect.', 'smwcfps' )
                        ),                
                    )
		);
                 
		return $shortcodes;
	}
}

new Shortcodes_Master_Wcfps;
